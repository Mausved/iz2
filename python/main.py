import math
import sys
import struct
#import matplotlib.pyplot as plt
import random as rd


def func(a, nu, time):
    return a * math.sin(2 * math.pi * nu * time)


# Stress-tests
def generate_R_peak(path_to_output_file, slices):
    testing_input_in_bytes = open(path_to_output_file, 'wb')
    #testing_input_in_txt = open(path_to_output_file + ".txt", 'w')
    a = rd.uniform(1, 1000)
    nu = rd.uniform(100, 500)
    time = rd.uniform(0.1, 10)
    print(a, nu, time)
    step = time / slices
    nums = []
    times = []
    for i in range(slices):
        if i % 1000000 == 0:
            print(i)
        #print(i)
        value_in_file = func(a, nu, step * i)
        nums.append(value_in_file)
        times.append(step * i)
        testing_input_in_bytes.write(struct.pack("f", value_in_file))
        #testing_input_in_txt.write("{0:.2f} ".format(value_in_file))


    #plt.title("Generator of R_peaks")

    #plt.xlabel("time, s")
    #plt.ylabel("value, -")
    #plt.plot(times, nums, color='green', linestyle='dashed', linewidth=1)
    #plt.plot(times, nums)
    #plt.savefig(path_to_output_file + ".graph.png")
    testing_input_in_bytes.close()
    # plt.show()

# inputs for unit_tests
def test_inputs():
    testing_input_in_bytes0 = open("countPeaks_0.bin", "wb")
    testing_input_in_bytes1 = open("countPeaks_1.bin", "wb")
    testing_input_in_bytes3 = open("countPeaks_3.bin", "wb")
    testing_input_in_bytes5 = open("countPeaks_5.bin", "wb")
    inputs0 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    inputs1 = [-1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1]
    inputs3 = [1, 2, 3, 2, 1, -2, -3, -2, -1, 2, 3, 2, 1]
    inputs5 = [1, 2, 3, 2, 1, 2, 3, 2, 1, 2, 3, 2, 1]

    for value in inputs0:
        testing_input_in_bytes0.write(struct.pack("f", value))
    for value in inputs1:
        testing_input_in_bytes1.write(struct.pack("f", value))
    for value in inputs3:
        testing_input_in_bytes3.write(struct.pack("f", value))
    for value in inputs5:
        testing_input_in_bytes5.write(struct.pack("f", value))
    for value in inputs_2values:
        testing_input_in_bytes_2values.write(struct.pack("f", value))


if __name__ == '__main__':
    #test_inputs()
    path1 = sys.argv[1]
    size_of_data = int(sys.argv[2])
    generate_R_peak(path1, size_of_data)
    print("Python script is done\n")
