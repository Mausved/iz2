#ifndef GENERATOR_H
#define GENERATOR_H
#include <stdbool.h>
#include <stdio.h>

#include "utils.h"
#define DEFAULT_R_window 115
#define DEFAULT_size_of_data 100000000

typedef struct Input_response {
  FILE* input_file;
  size_t file_size;
  bool ok;
  bool is_error_flag;
  bool is_get_help;
  bool is_multiprocessing;
} Input_response;

int finding_multiprocessing(Input_response* input);
int check_file(Input_response* input, char file_path[MAX_FILEPATH_LENGHT]);
int check_input(char file_path[MAX_FILEPATH_LENGHT], Input_response* input);
int get_input(int argc, char** argv, Input_response* input,
              char file_path[MAX_FILEPATH_LENGHT]);
int generate_input(char* path_to_input_file, bool need_to_generate_python,
                   const int size_of_data);
int get_python_input_file(int size_of_data, char* path_to_python_input_file,
                          char* python_source_dir);
int run_serial_example(int argc, char* argv[], const int size_of_data);
int run_with_file(int argc, char* argv[]);
#endif  // GENERATOR_H
