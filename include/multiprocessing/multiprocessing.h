#ifndef MULTIPROCESSING_MULTIPROCESSING_H
#define MULTIPROCESSING_MULTIPROCESSING_H
#include <stdio.h>
int run_searching_peaks(float* const array, size_t size, int* const R_window);
#endif  // MULTIPROCESSING_MULTIPROCESSING_H
