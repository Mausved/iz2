#ifndef SERIAL_SERIAL_H
#define SERIAL_SERIAL_H
#include <stdio.h>
int run_searching_peaks(float* const array, size_t size, int* const R_window);
#endif  // SERIAL_SERIAL_H
