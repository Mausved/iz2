#ifndef GENERALDEFINITIONS_H
#define GENERALDEFINITIONS_H
#define PYTHON_COMMAND "python3"
#define PYTHON_SCRIPT_NAME "main.py"
#define PYTHON_SOURCE_DIR "python/"
#define PYTHON_INPUT_FILE_NAME "testing_input.bin"
#define MAX_FILEPATH_LENGHT 100
#define MAX_ARGS_LENGHT 100
#define DECIMAL_NUMBER_SYSTEM 10
#define MAX_PYTHON_SCRIPT_LENGHT (2 * MAX_ARGS_LENGHT + MAX_FILEPATH_LENGHT)
#define unlikely(expr) __builtin_expect(!!(expr), 0)
#define likely(expr) __builtin_expect(!!(expr), 1)

#include <stdbool.h>
#include <stdio.h>

int get_file_size(FILE* const input_file);

int search_peaks(float* const array, size_t size, char* const R_window_string,
                 int(find_peaks_in_mode(float* const, size_t n, int* const)));
int finding_local_max_min(const float must_be_greater,
                          const float must_be_lesser, int const* R_window,
                          int* count_window, int* peaks,
                          bool* const what_needed_to_be_found,
                          bool* const what_to_look_for_now);
int is_this_a_peak(const int* const R_window, bool* const it_first_value,
                   float* const this_value, const float* const next_value,
                   bool* const finding_local_max, bool* const finding_local_min,
                   int* const count_window);

#endif  // GENERALDEFINITIONS_H
