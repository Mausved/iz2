#include <dlfcn.h>
#include <sys/mman.h>
#include <sys/sysinfo.h>

#include "gtest/gtest.h"
#define TEST_SOURCE_DIR "./test/inputs/"
#define TEST_FILE_WITH_SIZE_52 "countPeaks_5.bin"
#define TEST_FILE_COUNT_PEAKS_30_MULTIPROCESSING "countPeaks_30.bin"
#define TEST_FILE_WITH_ERROR_MUNMAP "error_to_munmap.bin"
extern "C" {
#include "multiprocessing.h"
#include "utils.h"
}

namespace TESTS_get_file_size {
FILE* input_file = NULL;
char path_to_input_file[MAX_FILEPATH_LENGHT];

TEST(get_file_size, expectedError) { EXPECT_EQ(get_file_size(NULL), -1); }
TEST(get_file_size, success_get_file_size) {
  snprintf(path_to_input_file, MAX_FILEPATH_LENGHT, "%s%s", TEST_SOURCE_DIR,
           TEST_FILE_WITH_SIZE_52);
  input_file = fopen(path_to_input_file, "rb");
  EXPECT_EQ(get_file_size(input_file), 52);
  fclose(input_file);
}
}  // namespace TESTS_get_file_size

namespace TESTS_multiprocessing {
float array_with_max_trend[] = {1, 2, 3, 4, 4, 4, 5, 6, 7};
float array_with_min_trend[] = {5, 6, 7, 4, 4, 4, 3, 2, 1};
int current_number_of_process = 1;
int start_pos_for_current_process = 5;
bool finding_local_min = false;
bool finding_local_max = false;
bool it_first_value = true;
float this_value = 0;
TEST(run_searching_peaks, nullPtr) {
  EXPECT_EQ(run_searching_peaks(NULL, 0, NULL), -1);
}
TEST(run_searching_peaks, expectedError_statusMinusOne_OnlyWritePerms) {
  int R_window = 1;
  FILE* input_file = NULL;
  char path_to_input_file[MAX_FILEPATH_LENGHT] = "";
  snprintf(path_to_input_file, MAX_FILEPATH_LENGHT, "%s%s", TEST_SOURCE_DIR,
           TEST_FILE_COUNT_PEAKS_30_MULTIPROCESSING);
  input_file = fopen(path_to_input_file, "ab");
  int fd = fileno(input_file);
  float* array = (float*)mmap(NULL, get_file_size(input_file), PROT_READ,
                              MAP_POPULATE | MAP_SHARED, fd, 0);
  size_t size = get_file_size(input_file) / (sizeof(*array));
  if (input_file && array) {
    int (*multiprocessing_search)(float* const file_name, size_t,
                                    int* const R_window);
    void* library = NULL;
    library = dlopen("libmultiprocessing_lib.so", RTLD_LAZY);
    if (library) {
        multiprocessing_search = (int (*)(float* const, size_t, int* const))(
              dlsym(library, "run_searching_peaks"));
        EXPECT_EQ(multiprocessing_search(array, size, &R_window), -1);
        dlclose(library);
    }
  }
  munmap(array, get_file_size(input_file));
  fclose(input_file);
}
TEST(run_searching_peaks, noError) {
  int R_window = 1;
  FILE* input_file = NULL;
  char path_to_input_file[MAX_FILEPATH_LENGHT] = "";
  snprintf(path_to_input_file, MAX_FILEPATH_LENGHT, "%s%s", TEST_SOURCE_DIR,
           TEST_FILE_COUNT_PEAKS_30_MULTIPROCESSING);
  input_file = fopen(path_to_input_file, "rb");
  int fd = fileno(input_file);
  float* array = (float*)mmap(NULL, get_file_size(input_file), PROT_READ,
                              MAP_SHARED | MAP_POPULATE, fd, 0);
  size_t size = get_file_size(input_file) / sizeof(*(array));
  if (input_file && array) {
    EXPECT_EQ(run_searching_peaks(array, size, &R_window), 30);
  }
  munmap(array, get_file_size(input_file));
  fclose(input_file);
}

}  // namespace TESTS_multiprocessing
