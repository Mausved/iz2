#include <sys/mman.h>

#include <string>

#include "gtest/gtest.h"
#define TEST_SOURCE_DIR "./test/inputs/"
#define PEAKS_0 "countPeaks_0.bin"
#define PEAKS_1 "countPeaks_1.bin"
#define PEAKS_3 "countPeaks_3.bin"
#define PEAKS_5 "countPeaks_5.bin"
extern "C" {
#include <unistd.h>

#include "./inputFuncs.h"
#include "serial.h"
#include "utils.h"
}

namespace TESTS_finding_local_max_min {

float must_be_greater;
float must_be_lesser;
int* R_window;
int* count_window;
int* peaks;
bool* needed_to_find_max;
bool* needed_to_find_min;

TEST(finding_local_max_min, expectedMinusOne) {
  must_be_greater = 0;
  must_be_lesser = 0;
  R_window = NULL;
  count_window = NULL;
  peaks = NULL;
  needed_to_find_max = NULL;
  needed_to_find_min = NULL;
  EXPECT_EQ(finding_local_max_min(must_be_greater, must_be_lesser, R_window,
                                  count_window, peaks, needed_to_find_max,
                                  needed_to_find_min),
            -1);
}

TEST(finding_local_max_min, findingMax) {
  R_window = new int;
  count_window = new int;
  peaks = new int;
  needed_to_find_max = new bool;
  needed_to_find_min = new bool;

  must_be_greater = 5.0;
  must_be_lesser = 3.0;
  *R_window = 150;
  *count_window = 250;
  *peaks = 10;
  *needed_to_find_max = true;
  *needed_to_find_min = false;

  EXPECT_EQ(finding_local_max_min(must_be_greater, must_be_lesser, R_window,
                                  count_window, peaks, needed_to_find_max,
                                  needed_to_find_min),
            0);
  EXPECT_EQ(must_be_greater, 5.0);
  EXPECT_EQ(must_be_lesser, 3.0);
  EXPECT_EQ(*R_window, 150);
  EXPECT_EQ(*count_window, 0);
  EXPECT_EQ(*peaks, 11);
  EXPECT_EQ(*needed_to_find_max, false);
  EXPECT_EQ(*needed_to_find_min, true);
}

TEST(finding_local_max_min, findingMin) {
  EXPECT_EQ(finding_local_max_min(must_be_greater, must_be_lesser, R_window,
                                  count_window, peaks, needed_to_find_min,
                                  needed_to_find_max),
            0);
  EXPECT_EQ(must_be_greater, 5.0);
  EXPECT_EQ(must_be_lesser, 3.0);
  EXPECT_EQ(*R_window, 150);
  EXPECT_EQ(*count_window, 0);
  EXPECT_EQ(*peaks, 11);
  EXPECT_EQ(*needed_to_find_max, true);
  EXPECT_EQ(*needed_to_find_min, false);
}

TEST(finding_local_max_min, notEnoughtWindow) {
  *count_window = *R_window;
  EXPECT_EQ(finding_local_max_min(must_be_greater, must_be_lesser, R_window,
                                  count_window, peaks, needed_to_find_max,
                                  needed_to_find_min),
            0);
  EXPECT_EQ(must_be_greater, 5.0);
  EXPECT_EQ(must_be_lesser, 3.0);
  EXPECT_EQ(*R_window, 150);
  EXPECT_EQ(*count_window, 0);
  EXPECT_EQ(*peaks, 12);
  EXPECT_EQ(*needed_to_find_max, false);
  EXPECT_EQ(*needed_to_find_min, true);
}

TEST(finding_local_max_min, swapGreaterAndLesser) {
  std::swap(must_be_greater, must_be_lesser);
  EXPECT_EQ(finding_local_max_min(must_be_greater, must_be_lesser, R_window,
                                  count_window, peaks, needed_to_find_min,
                                  needed_to_find_max),
            0);
  EXPECT_EQ(must_be_greater, 3.0);
  EXPECT_EQ(must_be_lesser, 5.0);
  EXPECT_EQ(*R_window, 150);
  EXPECT_EQ(*count_window, 0);
  EXPECT_EQ(*peaks, 12);
  EXPECT_EQ(*needed_to_find_max, false);
  EXPECT_EQ(*needed_to_find_min, true);

  delete R_window;
  delete count_window;
  delete peaks;
  delete needed_to_find_max;
  delete needed_to_find_min;
}
}  // namespace TESTS_finding_local_max_min

namespace TESTS_serial {
static float* array = NULL;
static char path_to_input_file[MAX_FILEPATH_LENGHT] = "";
static int* R_window;
TEST(run_searching_peaks, expectedError) {
  EXPECT_EQ(is_this_a_peak(NULL, NULL, NULL, NULL, NULL, NULL, NULL), -1);
}

TEST(run_searching_peaks, countPeaks_0) {
  R_window = new int;
  *R_window = 1;
  snprintf(path_to_input_file, MAX_FILEPATH_LENGHT, "%s%s", TEST_SOURCE_DIR,
           PEAKS_0);
  FILE* input_file = fopen(path_to_input_file, "rb");
  int fd = fileno(input_file);
  array = (float*)mmap(NULL, get_file_size(input_file), PROT_READ,
                       MAP_PRIVATE | MAP_POPULATE, fd, 0);
  size_t size = get_file_size(input_file) / sizeof(*array);
  EXPECT_EQ(run_searching_peaks(array, size, R_window), 0);
  munmap(array, get_file_size(input_file));
  fclose(input_file);
}
TEST(run_searching_peaks, countPeaks_1) {
  snprintf(path_to_input_file, MAX_FILEPATH_LENGHT, "%s%s", TEST_SOURCE_DIR,
           PEAKS_1);
  FILE* input_file = fopen(path_to_input_file, "rb");
  int fd = fileno(input_file);
  array = (float*)mmap(NULL, get_file_size(input_file), PROT_READ,
                       MAP_PRIVATE | MAP_POPULATE, fd, 0);
  size_t size = get_file_size(input_file) / sizeof(*array);
  EXPECT_EQ(run_searching_peaks(array, size, R_window), 1);
  munmap(array, get_file_size(input_file));
  fclose(input_file);
}
TEST(run_searching_peaks, countPeaks_3) {
  snprintf(path_to_input_file, MAX_FILEPATH_LENGHT, "%s%s", TEST_SOURCE_DIR,
           PEAKS_3);
  FILE* input_file = fopen(path_to_input_file, "rb");
  int fd = fileno(input_file);
  array = (float*)mmap(NULL, get_file_size(input_file), PROT_READ,
                       MAP_PRIVATE | MAP_POPULATE, fd, 0);
  size_t size = get_file_size(input_file) / sizeof(*array);
  EXPECT_EQ(run_searching_peaks(array, size, R_window), 3);
  munmap(array, get_file_size(input_file));
  fclose(input_file);
}
TEST(run_searching_peaks, countPeaks_5) {
  snprintf(path_to_input_file, MAX_FILEPATH_LENGHT, "%s%s", TEST_SOURCE_DIR,
           PEAKS_5);
  FILE* input_file = fopen(path_to_input_file, "rb");
  int fd = fileno(input_file);
  array = (float*)mmap(NULL, get_file_size(input_file), PROT_READ,
                       MAP_PRIVATE | MAP_POPULATE, fd, 0);
  size_t size = get_file_size(input_file) / sizeof(*array);
  EXPECT_EQ(run_searching_peaks(array, size, R_window), 5);
  munmap(array, get_file_size(input_file));
  fclose(input_file);
  delete R_window;
}
TEST(run_searching_peaks, expected_minus_one) {
  FILE* input_file = fopen(path_to_input_file, "rb");
  int fd = fileno(input_file);
  array = (float*)mmap(NULL, get_file_size(input_file), PROT_READ,
                       MAP_PRIVATE | MAP_POPULATE, fd, 0);
  size_t size = get_file_size(input_file) / sizeof(*array);
  R_window = NULL;
  EXPECT_EQ(run_searching_peaks(array, size, R_window), -1);
  munmap(array, get_file_size(input_file));
  fclose(input_file);
}
}  // namespace TESTS_serial
