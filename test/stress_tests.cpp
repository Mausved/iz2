#include <gtest/gtest.h>
extern "C" {
#include "inputFuncs.h"
#include "utils.h"
}

TEST(values_100000000, serial_with_generate) {
    int argc = 3;
    char filename[MAX_FILEPATH_LENGHT] = "";
    char* argv[] = {(char*)"./main", (char*)"-f", filename, NULL};
    const int size_of_data = DEFAULT_size_of_data;
    EXPECT_EXIT(run_serial_example(argc, argv, size_of_data),
                ::testing::ExitedWithCode(0), ".*");

}

TEST(values_100000000, multiprocessing_use_file_before) {
    int argc = 4;
    char path_to_input_file[MAX_FILEPATH_LENGHT] = "";
    snprintf(path_to_input_file, MAX_FILEPATH_LENGHT, "./%s%s", PYTHON_SOURCE_DIR,
             PYTHON_INPUT_FILE_NAME);
    char* argv[] = {(char*)"./main", (char*)"-p", (char*)"-f",
                    path_to_input_file, NULL};
    EXPECT_EXIT(run_with_file(argc, argv), ::testing::ExitedWithCode(0), ".*");
}
