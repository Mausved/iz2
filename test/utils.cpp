#include <sys/mman.h>

#include "gtest/gtest.h"
#define TEST_SOURCE_DIR "./test/inputs/"
#define WHERE_TEST_PEAKS_COUNTING "countPeaks_5.bin"
#define WHERE_TEST_PEAKS_WITHOUT_TREND "countPeaks_0.bin"
extern "C" {
#include "serial.h"
#include "utils.h"
}

namespace TESTS_search_peaks {
float* array = NULL;
char path_to_input_file[MAX_FILEPATH_LENGHT];
char R_window_string[MAX_FILEPATH_LENGHT];

TEST(search_peaks, expectedError) {
  EXPECT_EQ(search_peaks(NULL, 0, NULL, NULL), -1);
}
TEST(search_peaks, successFindindPeaks_withWindow2_serial) {
  snprintf(R_window_string, sizeof(R_window_string), "5");
  snprintf(path_to_input_file, MAX_FILEPATH_LENGHT, "%s%s", TEST_SOURCE_DIR,
           WHERE_TEST_PEAKS_COUNTING);
  FILE* input_file = fopen(path_to_input_file, "rb");
  int fd = fileno(input_file);
  array = (float*)mmap(NULL, get_file_size(input_file), PROT_READ,
                       MAP_SHARED | MAP_POPULATE, fd, 0);
  size_t size = get_file_size(input_file) / sizeof(*array);
  EXPECT_EQ(search_peaks(array, size, R_window_string, run_searching_peaks), 2);
  munmap(array, get_file_size(input_file));
  fclose(input_file);
}
}  // namespace TESTS_search_peaks
