#include "gtest/gtest.h"
#define TEST_SOURCE_DIR "./test/inputs/"
#define TEST_FILE_COUNT_PEAKS_9 "countPeaks_9.bin"
extern "C" {
#include "./inputFuncs.h"
}

namespace TESTS_finding_multiprocessing {
TEST(finding_multiprocessing, nullPtr) {
  EXPECT_EQ(finding_multiprocessing(NULL), -1);
}
}  // namespace TESTS_finding_multiprocessing

namespace TESTS_check_file {
TEST(check_file, nullPtr) { EXPECT_EQ(check_file(NULL, NULL), -1); }
Input_response* input;
char file_path[MAX_FILEPATH_LENGHT] = "";

TEST(check_file, nullPathLength) {
  input = new Input_response;
  EXPECT_EQ(check_file(input, file_path), 0);
  EXPECT_EQ(input->ok, false);
}

TEST(check_file, pathNotExist) {
  snprintf(file_path, sizeof(file_path), "%s", "unknown_path");
  EXPECT_EQ(check_file(input, file_path), 0);
  EXPECT_EQ(input->ok, false);
}

TEST(check_file, pathExist) {
  snprintf(file_path, sizeof(file_path), "%s%s", PYTHON_SOURCE_DIR,
           PYTHON_SCRIPT_NAME);
  EXPECT_EQ(check_file(input, file_path), 0);
  EXPECT_EQ(input->ok, true);
  delete input;
}
}  // namespace TESTS_check_file

namespace TESTS_check_input {
TEST(check_input, nullPtr) { EXPECT_EQ(check_input(NULL, NULL), -1); }
Input_response* input;
char file_path[MAX_FILEPATH_LENGHT] = "";
TEST(check_input, getHelp) {
  input = new Input_response;
  input->input_file = NULL;
  input->file_size = 0;
  input->is_error_flag = false;
  input->is_get_help = true;
  input->is_multiprocessing = false;
  EXPECT_EQ(check_input(file_path, input), 0);
}

TEST(check_input, notGetHelp_butFileNotExist) {
  input->is_get_help = false;
  snprintf(file_path, sizeof(file_path), "%s", "unknown_path");
  EXPECT_EQ(check_input(file_path, input), -1);
}

TEST(check_input, notGetHelp_andFileExist) {
  snprintf(file_path, sizeof(file_path), "%s%s", PYTHON_SOURCE_DIR,
           PYTHON_SCRIPT_NAME);
  EXPECT_EQ(check_input(file_path, input), 0);
  delete input;
}
}  // namespace TESTS_check_input

namespace TESTS_get_input {
int argc;
char** argv = NULL;
char filePath[MAX_FILEPATH_LENGHT];
char flag[MAX_ARGS_LENGHT];
char executable[MAX_ARGS_LENGHT] = "./main";

char** initArgv(int argc, char** argv) {
  if (argv == NULL) {
    argv = new char*[argc];
    for (int i = 0; i < argc; i++) {
      argv[i] = new char[MAX_ARGS_LENGHT];
    }
  }
  return argv;
}

char** deleteArgv(int argc, char** argv) {
  if (argv != NULL) {
    for (int i = 0; i < argc; i++) {
      delete[] argv[i];
    }
    delete[] argv;
  }
  return NULL;
}

char** updateArgv(int argcInput, char** argv) {
  argv = deleteArgv(argc, argv);
  argc = argcInput;
  argv = initArgv(argc, argv);
  return argv;
}

void testing(int argc, char** argv, char* flag, bool expectedMultiprocessing,
             bool expectedOk, char filePath[MAX_FILEPATH_LENGHT],
             char executable[MAX_FILEPATH_LENGHT], int expectedEQ) {
  Input_response input;
  snprintf(argv[0], MAX_ARGS_LENGHT, "%s", executable);
  snprintf(argv[1], MAX_ARGS_LENGHT, "%s", flag);
  EXPECT_EQ(get_input(argc, argv, &input, filePath), expectedEQ);
  EXPECT_EQ(input.is_multiprocessing, expectedMultiprocessing);
  EXPECT_EQ(input.ok, expectedOk);
}

TEST(get_input, nullPtr) { EXPECT_EQ(get_input(0, NULL, NULL, NULL), -1); }
TEST(get_input, getHelp) {
  argv = updateArgv(2, argv);
  snprintf(flag, sizeof(flag), "%s", "-h");
  testing(argc, argv, flag, false, false, filePath, executable, 0);
}
TEST(get_input, doSerial_withoutFile) {
  snprintf(flag, sizeof(flag), "%s", "-f");
  testing(argc, argv, flag, false, false, filePath, executable, -1);
}
TEST(get_input, doMultiprocessing_withoutFile) {
  snprintf(flag, sizeof(flag), "%s", "-p");
  testing(argc, argv, flag, true, false, filePath, executable, -1);
}
TEST(get_input, doSerial_withFile) {
  argv = updateArgv(3, argv);
  snprintf(flag, sizeof(flag), "%s", "-f");
  snprintf(argv[2], MAX_ARGS_LENGHT, "%s%s", PYTHON_SOURCE_DIR,
           PYTHON_SCRIPT_NAME);
  testing(argc, argv, flag, false, true, filePath, executable, 0);
}
TEST(get_input, doMultiprocessing_withFile) {
  argv = updateArgv(4, argv);
  snprintf(flag, sizeof(flag), "%s", "-p");
  snprintf(argv[2], MAX_ARGS_LENGHT, "%s", "-f");
  snprintf(argv[3], MAX_ARGS_LENGHT, "%s%s", PYTHON_SOURCE_DIR,
           PYTHON_SCRIPT_NAME);
  testing(argc, argv, flag, true, true, filePath, executable, 0);
}
TEST(get_input, errorFlags) {
  argv = updateArgv(2, argv);
  snprintf(flag, sizeof(flag), "%s", "-e");
  testing(argc, argv, flag, false, false, filePath, executable, -1);
  argv = deleteArgv(argc, argv);
}
}  // namespace TESTS_get_input

namespace TESTS_generate_input {
TEST(generate_input, nullPtr) { EXPECT_EQ(generate_input(NULL, false, 0), -1); }
TEST(generate_input, expectedNoError_noGenerate) {
  char path_to_input_file[MAX_FILEPATH_LENGHT] = "";
  bool need_to_generate_by_python = false;
  const int size_of_data = 1000;
  EXPECT_EQ(generate_input(path_to_input_file, need_to_generate_by_python,
                           size_of_data),
            0);
  remove(path_to_input_file);
}
TEST(generate_input, expectedNoError_Generate) {
  char path_to_input_file[MAX_FILEPATH_LENGHT] = "";
  bool need_to_generate_by_python = true;
  const int size_of_data = 1000;
  EXPECT_EQ(generate_input(path_to_input_file, need_to_generate_by_python,
                           size_of_data),
            0);
  remove(path_to_input_file);
}
}  // namespace TESTS_generate_input

namespace TESTS_get_python_input_file {
int size_of_data = 100;
TEST(get_python_input_file, nullPtr) {
  EXPECT_EQ(get_python_input_file(size_of_data, NULL, NULL), -1);
}

TEST(get_python_input_file, generateSimpleFile) {
  char path_to_python_input_file[MAX_FILEPATH_LENGHT] = "";
  snprintf(path_to_python_input_file, sizeof(path_to_python_input_file),
           "./test/%s_%s", PYTHON_INPUT_FILE_NAME, "test");
  char path_to_python_source_dir[] = PYTHON_SOURCE_DIR;
  EXPECT_EQ(get_python_input_file(size_of_data, path_to_python_input_file,
                                  path_to_python_source_dir),
            0);
  remove(path_to_python_input_file);
}

}  // namespace TESTS_get_python_input_file

namespace TESTS_runs {
TEST(run_with_file, expectedError) {
  int argc = 3;
  char path_to_input_file[MAX_FILEPATH_LENGHT] = "";
  snprintf(path_to_input_file, MAX_FILEPATH_LENGHT, "%s%s", "errorPath",
           "errorPath");
  char* argv[] = {(char*)"./unit_tests", (char*)"null", path_to_input_file,
                  NULL};
  EXPECT_EQ(run_with_file(argc, argv), -1);
}

TEST(run_with_file, serial_expectedNoError) {
  int argc = 3;
  char path_to_input_file[MAX_FILEPATH_LENGHT] = "";
  snprintf(path_to_input_file, MAX_FILEPATH_LENGHT, "%s%s", TEST_SOURCE_DIR,
           TEST_FILE_COUNT_PEAKS_9);
  char* argv[] = {(char*)"./unit_tests", (char*)"-f", path_to_input_file, NULL};
  EXPECT_EXIT(run_with_file(argc, argv), ::testing::ExitedWithCode(0), ".*");
}

TEST(run_with_file, multiprocessing_expectedNoError) {
  int argc = 4;
  char path_to_input_file[MAX_FILEPATH_LENGHT] = "";
  snprintf(path_to_input_file, MAX_FILEPATH_LENGHT, "%s%s", TEST_SOURCE_DIR,
           TEST_FILE_COUNT_PEAKS_9);
  char* argv[] = {(char*)"./unit_tests", (char*)"-p", (char*)"-f",
                  path_to_input_file, NULL};
  EXPECT_EXIT(run_with_file(argc, argv), ::testing::ExitedWithCode(0), ".*");
}
}  // namespace TESTS_runs
