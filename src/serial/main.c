#include <sys/mman.h>
#include <time.h>

#include "./serial.h"
#include "./utils.h"

int main(int argc, char* argv[]) {
  clock_t start, end;
  start = clock();

  int peaks = 0;
  char* file_name = NULL;
  file_name = argv[1];
  char* R_window_string = NULL;
  R_window_string = argv[2];
  FILE* input_file = NULL;
  input_file = fopen(file_name, "rb");
  size_t file_size = get_file_size(input_file);
  int fd = fileno(input_file);
  float* array =
      mmap(NULL, file_size, PROT_READ, MAP_PRIVATE | MAP_POPULATE, fd, 0);
  size_t size = file_size / sizeof(*array);
  if (likely(input_file && array)) {
    peaks = search_peaks(array, size, R_window_string, run_searching_peaks);
    if (peaks == -1) {
      printf("error to find peaks in serial mode\n");
      munmap(array, get_file_size(input_file));
      fclose(input_file);
      return -1;
    }
  } else {
    printf("error to reading file in serial mode\n");
    return -1;
  }
  printf("________summary peaks = %i_____________\n", peaks);
  munmap(array, get_file_size(input_file));
  fclose(input_file);
  end = clock() - start;
  double taken_time = ((double)end) / CLOCKS_PER_SEC;
  printf("Executed time in serial: %f\n", taken_time);
  return 0;
}
