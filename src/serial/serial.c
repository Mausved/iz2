#include "./serial.h"

#include "./utils.h"

int run_searching_peaks(float* const array, size_t size, int* const R_window) {
  int peaks = 0;
  if (likely(array != NULL && R_window != NULL)) {
    int count_window = 0;
    count_window = *R_window;
    float this_value = 0;
    bool it_first_value = true;
    bool finding_local_max = false;
    bool finding_local_min = false;
    for (size_t i = 0; i < size; i++) {
      peaks +=
          is_this_a_peak(R_window, &it_first_value, &this_value, &array[i],
                         &finding_local_max, &finding_local_min, &count_window);
      this_value = array[i];
    }
  } else {
    printf("error reading file in serial\n");
    return -1;
  }
  return peaks;
}
