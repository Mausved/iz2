#include "./multiprocessing.h"

#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/sysinfo.h>
#include <sys/wait.h>
#include <unistd.h>

#include "./utils.h"

#define MIN_POINTS_TO_NEED_IN_AREA 3
#define unlikely(expr) __builtin_expect(!!(expr), 0)
#define likely(expr) __builtin_expect(!!(expr), 1)

static int fd = 0;
static float* file_array_shared = NULL;
static int* peaks_array_shared = NULL;
static int* peaks_array_shared_size = NULL;
static pid_t* pid_list_shared = NULL;
static pthread_mutex_t* shared_mutex = NULL;
static size_t* pid_list_size_shared = NULL;
static size_t amount_max_processes = 0;
static size_t last_process_number = 0;
static size_t amount_of_values_in_one_process = 0;
static size_t amount_of_values_total = 0;
static size_t max_peaks = 0;

typedef struct Request {
  pid_t pid;
  int current_number_of_process;
} Request;

typedef struct Shared_data {
  pthread_mutex_t mutex;
} Shared_data;

static int check_mmap(void* const mmap) {
  if (unlikely(mmap == MAP_FAILED)) {
    return -1;
  }
  return 0;
}

static int check_munmap(void* const mmap, const int size) {
  if (unlikely(munmap(mmap, size) != 0)) {
    return -1;
  }
  return 0;
}

static void init_shared_mutex(int prot, int flags) {
  shared_mutex = mmap(NULL, sizeof(pthread_mutex_t), prot, flags, -1, 0);
  pthread_mutexattr_t attr;
  pthread_mutexattr_init(&attr);
  pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED);
  pthread_mutex_init(shared_mutex, &attr);
}

static int __init(float* const input_array, size_t array_size,
                  int* const R_window) {
  if (likely(input_array && R_window)) {
    file_array_shared = input_array;
    int prot = PROT_READ | PROT_WRITE;
    int flags = MAP_SHARED | MAP_ANONYMOUS;
    init_shared_mutex(prot, flags);

    amount_of_values_total = array_size;
    max_peaks = amount_of_values_total / *R_window;
    amount_max_processes = get_nprocs();

    last_process_number = amount_max_processes - 1;
    amount_of_values_in_one_process =
        amount_of_values_total / amount_max_processes;
    peaks_array_shared = mmap(NULL, sizeof(int[max_peaks]), prot, flags, -1, 0);
    peaks_array_shared_size = mmap(NULL, sizeof(int), prot, flags, -1, 0);

    pid_list_shared =
        mmap(NULL, sizeof(pid_t[amount_max_processes]), prot, flags, -1, 0);

    pid_list_size_shared = mmap(NULL, sizeof(size_t), prot, flags, -1, 0);

    *peaks_array_shared_size = 0;
    *pid_list_size_shared = 1;
    int status = check_mmap(shared_mutex) + check_mmap(file_array_shared) +
                 check_mmap(peaks_array_shared) + check_mmap(pid_list_shared) +
                 check_mmap(pid_list_size_shared);
    if (unlikely(status < 0)) {
      printf("mmap failed!\n");
      close(fd);
      return -1;
    }
    return 0;
  }
  printf("error to init\n");
  return -1;
}

static int __fini() {
  int status = 0;
  pthread_mutex_destroy(shared_mutex);
  size_t file_size = 0;
  file_size = amount_of_values_total * sizeof(*file_array_shared);
  status += check_munmap(file_array_shared, file_size) +
            check_munmap(peaks_array_shared, sizeof(int[max_peaks])) +
            check_munmap(peaks_array_shared_size, sizeof(int)) +
            check_munmap(pid_list_shared, sizeof(pid_t[amount_max_processes])) +
            check_munmap(pid_list_size_shared, sizeof(size_t));
  if (unlikely(status < 0)) {
    close(fd);
    printf("error to munmap\n");
    return -1;
  }
  close(fd);
  return 0;
}

static int compare_pos_peaks(const int* x, const int* y) { return (*x - *y); }

static int get_previous_trend(float* const file_array_shared,
                              const int* const current_number_of_process,
                              const int* const start_pos_for_current_process,
                              bool* const finding_local_min,
                              bool* const finding_local_max,
                              bool* const it_first_value,
                              float* const this_value) {
  if (unlikely(file_array_shared && current_number_of_process &&
               start_pos_for_current_process && finding_local_min &&
               finding_local_max && it_first_value && this_value)) {
    if (likely(*current_number_of_process > 0)) {
      for (size_t i = *start_pos_for_current_process;
           (i - 1) != 0 && (*finding_local_max + *finding_local_min < 1); i--) {
        float previous_value = 0;
        *this_value = file_array_shared[i];
        previous_value = file_array_shared[i - 1];
        if (*this_value > previous_value) {
          *finding_local_max = true;
          *finding_local_min = !*finding_local_max;
          *it_first_value = false;
        } else if (*this_value < previous_value) {
          *finding_local_min = true;
          *finding_local_max = !*finding_local_min;
          *it_first_value = false;
        }
      }
    }
    return 0;
  }
  printf("error to finding trend\n");
  return -1;
}

static Request parent_create_processes(pid_t* pid_list_shared,
                                       size_t* pid_list_size_shared,
                                       size_t amount_max_processes) {
  Request request = {0, 0};
  if (likely(pid_list_shared && pid_list_size_shared && amount_max_processes)) {
    pid_t pid = getpid();
    pid_list_shared[0] = pid;
    request.pid = pid;
    request.current_number_of_process = 0;
    for (size_t i = 1; i < amount_max_processes; i++) {
      pid = fork();
      if (pid == 0) {
        request.pid = pid;
        request.current_number_of_process = i;
        pid_list_shared[(*pid_list_size_shared)++] = getpid();
        return request;
      }
    }
    return request;
  }
  printf("error to create processes\n");
  request.current_number_of_process = -1;
  request.pid = -1;
  return request;
}

static int set_start_end_pos_for_process(int* start_pos_for_current_process,
                                         int* end_pos_for_current_process,
                                         int* current_number_of_process) {
  if (likely(start_pos_for_current_process && end_pos_for_current_process &&
             current_number_of_process)) {
    *start_pos_for_current_process =
        *current_number_of_process * amount_of_values_in_one_process;
    *end_pos_for_current_process =
        *start_pos_for_current_process + amount_of_values_in_one_process;
    if (*current_number_of_process == last_process_number) {
      *end_pos_for_current_process = amount_of_values_total;
    }
    return 0;
  }
  printf("error to set positions for processes\n");
  return -1;
}

static int processes_count_all_peaks(int* start_pos_for_current_process,
                                     int* end_pos_for_current_process,
                                     float* this_value, float* next_value,
                                     bool* finding_local_min,
                                     bool* finding_local_max,
                                     bool* it_first_value, int* R_window,
                                     int* count_window_in_process) {
  if (likely(start_pos_for_current_process && end_pos_for_current_process &&
             this_value && next_value && finding_local_min &&
             finding_local_max && it_first_value && R_window &&
             count_window_in_process)) {
    for (size_t i = *start_pos_for_current_process;
         i < *end_pos_for_current_process; i++) {
      if (likely(*end_pos_for_current_process -
                     *start_pos_for_current_process >=
                 MIN_POINTS_TO_NEED_IN_AREA)) {
        *this_value = file_array_shared[i];
        if (likely(i != (amount_of_values_total - 1))) {
          *next_value = file_array_shared[i + 1];
        }
        if (!(*finding_local_max || *finding_local_min) &&
            unlikely(i == *start_pos_for_current_process)) {
          *it_first_value = true;
        }
        if (is_this_a_peak(R_window, it_first_value, this_value, next_value,
                           finding_local_max, finding_local_min,
                           count_window_in_process)) {
          pthread_mutex_lock(shared_mutex);
          peaks_array_shared[(*peaks_array_shared_size)++] = i;
          pthread_mutex_unlock(shared_mutex);
        }
      }
    }
    return 0;
  }
  printf("error to count all peaks\n");
  return -1;
}

static void parent_wait_childs(pid_t pid) {
  if (pid == 0) {
    close(fd);
    exit(EXIT_SUCCESS);
  }

  for (size_t i = 1; i <= last_process_number; i++) {
    waitpid(pid_list_shared[i], EXIT_SUCCESS, WUNTRACED);
  }
}

static int count_summary_peaks_by_parent(int* R_window) {
  if (likely(R_window)) {
    int peaks = 0;
    for (int i = 0; i < *peaks_array_shared_size; i++) {
      if (likely(i > 0)) {
        if (peaks_array_shared[i] - peaks_array_shared[i - 1] >= *R_window) {
          peaks++;
        }
      } else {
        peaks++;
      }
    }
    return peaks;
  }
  printf("error to count peaks summary\n");
  return -1;
}

int run_searching_peaks(float* const array, size_t size, int* const R_window) {
  if (likely(array && R_window)) {
    int status = 0;
    status = __init(array, size, R_window);
    if (unlikely(status == -1)) {
      __fini();
      return -1;
    }
    int current_number_of_process = 0;
    pid_t pid = 0;
    Request req = parent_create_processes(pid_list_shared, pid_list_size_shared,
                                          amount_max_processes);
    current_number_of_process = req.current_number_of_process;
    pid = req.pid;

    int start_pos_for_current_process = 0;
    int end_pos_for_current_process = 0;
    set_start_end_pos_for_process(&start_pos_for_current_process,
                                  &end_pos_for_current_process,
                                  &current_number_of_process);

    int count_window_in_process = *R_window;
    bool finding_local_min = false;
    bool finding_local_max = false;
    bool it_first_value = false;
    float next_value = 0;
    float this_value = 0;

    get_previous_trend(file_array_shared, &current_number_of_process,
                       &start_pos_for_current_process, &finding_local_min,
                       &finding_local_max, &it_first_value, &this_value);

    if (likely(start_pos_for_current_process) != 0) {
      start_pos_for_current_process--;
    }

    processes_count_all_peaks(
        &start_pos_for_current_process, &end_pos_for_current_process,
        &this_value, &next_value, &finding_local_min, &finding_local_max,
        &it_first_value, R_window, &count_window_in_process);

    parent_wait_childs(pid);
    qsort(peaks_array_shared, *peaks_array_shared_size, sizeof(int),
          (int (*)(const void*, const void*))compare_pos_peaks);

    int peaks = 0;
    peaks = count_summary_peaks_by_parent(R_window);
    __fini();
    return peaks;
  } else {
    printf("error to readfile in multiprocessing\n");
    return -1;
  }
}
