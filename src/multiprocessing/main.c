#include <dlfcn.h>
#include <sys/mman.h>
#include <time.h>

#include "multiprocessing.h"
#include "utils.h"

int main(int argc, char* argv[]) {
  clock_t start, end;
  start = clock();
  void* library = NULL;
  int (*multiprocessing_search)(float* const file_name, size_t,
                                int* const R_window);
  library = dlopen("libmultiprocessing_lib.so", RTLD_LAZY);
  if (!library) {
    printf("error with get multiprocessing library\n");
    return -1;
  }

  multiprocessing_search = (int (*)(float* const, size_t, int* const))(
      dlsym(library, "run_searching_peaks"));

  int peaks = 0;
  char* file_path = NULL;
  file_path = argv[1];
  char* R_window_string = NULL;
  R_window_string = argv[2];
  FILE* input_file = NULL;
  input_file = fopen(file_path, "rb");
  float* array = NULL;
  int fd = fileno(input_file);
  array = mmap(NULL, get_file_size(input_file), PROT_READ,
               MAP_SHARED | MAP_POPULATE, fd, 0);
  if (input_file && array) {
    size_t size = get_file_size(input_file) / sizeof(*array);
    peaks = search_peaks(array, size, R_window_string, *multiprocessing_search);
  } else {
    printf("error to read file in multiprocessing mode");
    return -1;
  }

  if (peaks == -1) {
    printf("error to find peaks in multiprocessing mode\n");
    fclose(input_file);
    return -1;
  }
  printf("________summary peaks = %i_____________\n", peaks);
  fclose(input_file);
  dlclose(library);
  end = clock() - start;
  double taken_time = ((double)end) / CLOCKS_PER_SEC;
  printf("Executed time in multiprocessing: %f\n", taken_time);
  return 0;
}
