#include <unistd.h>

#include "./inputFuncs.h"

int main(int argc, char* argv[]) {
  if (argc == 1) {
    const int size_of_data = DEFAULT_size_of_data;
    return run_serial_example(argc, argv, size_of_data);
  } else {
    return run_with_file(argc, argv);
  }
}
