#include "./utils.h"

#include <stdlib.h>
#include <string.h>

int get_file_size(FILE* const input_file) {
  if (likely(input_file)) {
    size_t file_size = 0;
    fseek(input_file, 0L, SEEK_END);
    file_size = ftell(input_file);
    fseek(input_file, 0L, 0);
    return file_size;
  } else {
    printf("error to reading filesize in multiprocessing\n");
    return -1;
  }
}

int search_peaks(float* const array, size_t size, char* const R_window_string,
                 int(find_peaks_in_mode(float* const, size_t, int* const))) {
  if (likely(array && size && R_window_string)) {
    int peaks = 0;
    int R_window = 0;
    size_t end_pos = 0;
    end_pos = strlen(R_window_string) - 1;
    char* end = &R_window_string[end_pos];
    R_window = (int)strtol(R_window_string, &end, DECIMAL_NUMBER_SYSTEM);
    peaks = find_peaks_in_mode(array, size, &R_window);
    return peaks;
  } else {
    printf("error to search peaks\n");
    return -1;
  }
}

int finding_local_max_min(const float must_be_greater,
                          const float must_be_lesser, int const* R_window,
                          int* count_window, int* peaks,
                          bool* const what_needed_to_be_found,
                          bool* const what_to_look_for_now) {
  if (unlikely(!R_window || !count_window || !peaks ||
               !what_needed_to_be_found || !what_to_look_for_now)) {
    return -1;
  }

  else if (must_be_lesser <= must_be_greater) {
    *what_needed_to_be_found = false;
    *what_to_look_for_now = true;
    if (*count_window >= *R_window) {
      (*peaks)++;
      (*count_window) = 0;
    }
  }
  return 0;
}

int is_this_a_peak(const int* const R_window, bool* const it_first_value,
                   float* const this_value, const float* const next_value,
                   bool* const finding_local_max, bool* const finding_local_min,
                   int* const count_window) {
  if (R_window && this_value && next_value && finding_local_max &&
      finding_local_min && count_window) {
    int peaks = 0;
    if (unlikely(*it_first_value && *next_value != *this_value)) {
      if (*next_value > *this_value) {
        *finding_local_max = true;
      } else if (*next_value < *this_value) {
        *finding_local_max = false;
      }
      *finding_local_min = !(*finding_local_max);
      *it_first_value = false;
    } else if (*finding_local_max && (*next_value < *this_value)) {
      finding_local_max_min(*this_value, *next_value, R_window, count_window,
                            &peaks, finding_local_max, finding_local_min);
    } else if (*finding_local_min && (*next_value > *this_value)) {
      finding_local_max_min(*next_value, *this_value, R_window, count_window,
                            &peaks, finding_local_min, finding_local_max);
    }
    (*count_window)++;
    return peaks;
  }

  printf("error to check peak\n");
  return -1;
}
