#include "./inputFuncs.h"

#include <stdlib.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <unistd.h>

int finding_multiprocessing(Input_response* input) {
  if (likely(input != NULL)) {
    printf("Finding multiprocessing...");
    if (input->is_multiprocessing) {
      printf("YES(%i processes)\n", get_nprocs());
    } else {
      printf("NO\n");
    }
    return 0;
  }
  return -1;
}

int check_file(Input_response* input, char file_path[MAX_FILEPATH_LENGHT]) {
  if (likely(input != NULL && file_path != NULL)) {
    printf("File to open: ");
    if (strlen(file_path) == 0) {
      printf(
          "*NONE*\ntype file name after flag -f, or type -h flag to show help "
          "info\n");
      input->ok = false;
    } else {
      input->input_file = fopen(file_path, "rb");
      if (input->input_file != NULL) {
        printf("%s\n", file_path);
        input->ok = true;
        fclose(input->input_file);
      } else {
        printf("Path\"%s\" to file not found! Try again with valid path\n",
               file_path);
        input->ok = false;
      }
    }
    return 0;
  }
  printf("error to get input\n");
  return -1;
}

int check_input(char file_path[MAX_FILEPATH_LENGHT], Input_response* input) {
  if (likely(file_path != NULL && input != NULL)) {
    bool error = false;
    if (likely(!input->is_get_help)) {
      error = finding_multiprocessing(input) || check_file(input, file_path);
      if (!input->ok || error) {
        return -1;
      }
    }
    return 0;
  }
  printf("error to check input");
  return -1;
}

int get_input(int argc, char** argv, Input_response* input,
              char file_path[MAX_FILEPATH_LENGHT]) {
  if (likely(argc != 0 && argv != NULL && input != NULL && file_path != NULL)) {
    int opt = 0;
    char* opts = "-:hpf:";
    input->is_error_flag = false;
    input->ok = false;
    input->is_get_help = false;
    input->is_multiprocessing = false;
    input->input_file = NULL;

    while ((opt = getopt(argc, argv, opts)) != -1) {
      switch (opt) {
        case 'h': {
          printf(
              "Usage: %s [flags]\n-h - help\n-p - multiprocessing\n-f "
              "<filepath>\n"
              "run %s -p -f <filepath> to use multiprocessing for <filepath>\n"
              "if will not -p flag program will be launched in serial"
              "and will be generate input file by python script (default is "
              "10e+8 floats)\n",
              argv[0], argv[0]);
          input->is_get_help = true;
          break;
        }
        case 'p': {
          input->is_multiprocessing = true;
          break;
        }
        case 'f': {
          snprintf(file_path, MAX_FILEPATH_LENGHT, "%s", optarg);
          break;
        }
        default: {
          printf("type -h flag to show help\n");
          input->is_error_flag = true;
          break;
        }
      }
    }
    optind = 0;
    if (!input->is_error_flag) {
      return check_input(file_path, input);
    }
  }
  printf("error to get input\n");
  return -1;
}

int generate_input(char* path_to_input_file, bool need_to_generate_python,
                   const int size_of_data) {
  if (likely(path_to_input_file)) {
    snprintf(path_to_input_file, MAX_FILEPATH_LENGHT, "%s%s", PYTHON_SOURCE_DIR,
             PYTHON_INPUT_FILE_NAME);
    if (likely(need_to_generate_python)) {
      int response = 0;
      response = get_python_input_file(size_of_data, path_to_input_file,
                                       PYTHON_SOURCE_DIR);
      if (unlikely(response == -1)) {
        return -1;
      }
    }
    return 0;
  }
  printf("error to generate input file\n");
  return -1;
}

int get_python_input_file(int size_of_data, char* path_to_python_input_file,
                          char* python_source_dir) {
  if (path_to_python_input_file != NULL && python_source_dir != NULL) {
    char path_to_python_script[MAX_FILEPATH_LENGHT] = "";

    snprintf(path_to_python_script, sizeof(path_to_python_script), "%s%s",
             python_source_dir, PYTHON_SCRIPT_NAME);
    char args[MAX_ARGS_LENGHT] = "";
    char python_script[MAX_PYTHON_SCRIPT_LENGHT] = "";
    snprintf(args, MAX_ARGS_LENGHT, "%s %i", path_to_python_input_file,
             size_of_data);
    snprintf(python_script, sizeof(python_script), "%s %s %s", PYTHON_COMMAND,
             path_to_python_script, args);
    printf("python script = %s\n", python_script);
    system(python_script);
    return 0;
  }
  printf("error to generate file by python script\n");
  return -1;
}

int run_serial_example(int argc, char* argv[], const int size_of_data) {
  char path_to_input_file[MAX_FILEPATH_LENGHT] = "";
  bool need_to_generate_by_python = false;
  FILE* python_file = NULL;
  char path_to_python_file[MAX_FILEPATH_LENGHT] = "";
  snprintf(path_to_python_file, sizeof(path_to_python_file), "%s%s",
           PYTHON_SOURCE_DIR, PYTHON_INPUT_FILE_NAME);
  python_file = fopen(path_to_python_file, "rb");
  if (python_file == NULL) {
    need_to_generate_by_python = true;
  } else {
    need_to_generate_by_python = false;
    fclose(python_file);
  }
  if (generate_input(path_to_input_file, need_to_generate_by_python,
                     size_of_data) != -1) {
    execl(argv[0], argv[0], "-f", path_to_input_file, NULL);
  } else {
    printf("error to generate input\n");
    return -1;
  }
  return 0;
}

int run_with_file(int argc, char* argv[]) {
  Input_response response = {NULL, 0, false, false, false};
  char file_path[MAX_FILEPATH_LENGHT] = "";
  get_input(argc, argv, &response, file_path);

  if (likely(response.ok)) {
    printf("System is ready!\n");
  } else {
    printf("Program closing...\n");
    return -1;
  }
  char R_window_string[MAX_ARGS_LENGHT] = "";
  const char* path_to_input_file = argv[argc - 1];
  const int R_window = DEFAULT_R_window;

  printf("Enter the R_window: ");
  // fread(&R_window, sizeof(R_window), 1, stdin);
  // scanf("%i", &R_window);
  snprintf(R_window_string, sizeof(R_window_string), "%i", R_window);
  if (response.is_multiprocessing) {
    execl("./multiprocessing_executable", "./multiprocessing_executable",
          path_to_input_file, R_window_string, NULL);
  } else if (response.is_multiprocessing == false) {
    execl("./serial_executable", "./serial_executable", path_to_input_file,
          R_window_string, NULL);
  }
  return 0;
}
