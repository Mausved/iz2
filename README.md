[![pipeline status](https://gitlab.com/Mausved/iz2/badges/task/pipeline.svg)](https://gitlab.com/Mausved/iz2/-/commits/task)
[![coverage report](https://gitlab.com/Mausved/iz2/badges/task/coverage.svg)](https://gitlab.com/Mausved/iz2/-/commits/task)

## Name
IZ2

## Description
В вашем распоряжении — массив из 100 млн. чисел - отсчётов сигнала ЭКГ, полученных при частоте дискретизации f (например, f=360 Гц). Составьте последовательный алгоритм подсчета количества R-пиков сигнала, считая, что R-пики - это локальные максимумы сигнала по модулю (они могут быть как положительной, так и отрицательной полярности), и между соседними R-пиками должно пройти как минимум R_window отсчётов (например, R_window = 115), а затем распараллельте его на несколько процессов с учётом оптимизации работы с кэш-памятью.

## Visuals
Peaks are looked like:

![Good peaks](img/good_peaks.png)

But if peaks so many it can look like:

![Very a lot of peaks](img/many.png)

The peak is point on the top or bottom, look like:

![One peak is](img/so_close.png)

In tests peaks look like:

![Tests peaks](img/more_peaks.png)

## Installation
Default compile

## Usage
This program do serial or multiprocessing search of peaks in array. When compile, put "./main - h" to see information about usage.
Here is python script too, to generate data to program. Its generate random data.

## Authors and acknowledgment
Vladislav Maitsvetov
